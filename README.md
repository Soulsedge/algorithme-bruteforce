# Algorithme de brute force 

Algorithme de brute force jusqu'à 50 caractères avec un nombre inconnu de caractères et avancement de caractère par caractère

## Documentation : 
|Sommaire des fonctions                                                                                    |
| ------                                                                                                   |
|   [ajouterUn](#saisiechoix) - Augmente d'un caractère dans la place du message actuel                    |
|   [finPremierTableau](#menuchoixalgo) - Réalise les actions de fin de 1er tableau                        |
|   [actionAFaire](#demandermessage) - Cherche la bonne action à réaliser                                  |

### AjouterUn
```c
void ajouterUn(char* tryingString, int* USED_ARRAY, int USED_CHAR[], char SYMBOLE_LIST[]) 
```
Fonction ajouterUn qui augmente de un caractère dans la position actuel du message

>Parameters :
>
>   * char* tryingString - message en cours à augmenter
>    
>   * int* USED_ARRAY - tableau actuel utilisé
>
>   * int USED_CHAR[] - tableau des caractères à utiliser
>    
>   * char SYMBOLE_LIST[] - liste des caractères 

### FinPremierTableau
```c
void finPremierTableau(char* tryingString, int* USED_ARRAY, int USED_CHAR[], char SYMBOLE_LIST[], int* MAX_USED_ARRAY) 
```
Fonction ajouterUn qui augmente de un caractère dans la position actuel du message

>Parameters :
>
>   * char* tryingString - message en cours à augmenter
>    
>   * int* USED_ARRAY - tableau actuel utilisé
>
>   * int USED_CHAR[] - tableau des caractères à utiliser
>    
>   * char SYMBOLE_LIST[] - liste des caractères 
>    
>   * int* MAX_USED_ARRAY - Valeur du dernier tableau utilisé 

### ActionAFaire
```c
void actionAFaire(char* tryingString, int* USED_ARRAY, int USED_CHAR[], char SYMBOLE_LIST[], int* MAX_USED_ARRAY)
```
Fonction ajouterUn qui augmente de un caractère dans la position actuel du message

>Parameters :
>
>   * char* tryingString - message en cours à augmenter
>    
>   * int* USED_ARRAY - tableau actuel utilisé
>
>   * int USED_CHAR[] - tableau des caractères à utiliser
>    
>   * char SYMBOLE_LIST[] - liste des caractères 
>    
>   * int* MAX_USED_ARRAY - Valeur du dernier tableau utilisé 