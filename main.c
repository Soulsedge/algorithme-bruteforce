#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void ajouterUn(char* tryingString, int* USED_ARRAY, int USED_CHAR[], char SYMBOLE_LIST[]) 
{
	tryingString[*USED_ARRAY] = SYMBOLE_LIST[USED_CHAR[*USED_ARRAY]];
	USED_CHAR[*USED_ARRAY]++;	
}

void finPremierTableau(char* tryingString, int* USED_ARRAY, int USED_CHAR[], char SYMBOLE_LIST[], int* MAX_USED_ARRAY) 
{
	for (int i = 0; i <= *MAX_USED_ARRAY;i++) {
		USED_CHAR[i] = 0;
		ajouterUn(tryingString, &i, USED_CHAR, SYMBOLE_LIST);
	}
	*USED_ARRAY = *MAX_USED_ARRAY;
	*MAX_USED_ARRAY = *MAX_USED_ARRAY+1;
}

void actionAFaire(char* tryingString, int* USED_ARRAY, int USED_CHAR[], char SYMBOLE_LIST[], int* MAX_USED_ARRAY)
{
	//Si fin du premier tableau
	int toutcomplet = 0;
	for (int i = 0;i < *MAX_USED_ARRAY;i++) {
		if (tryingString[i] != SYMBOLE_LIST[strlen(SYMBOLE_LIST) - 2])
			toutcomplet = 1;
	}
	if ( toutcomplet == 0 ) finPremierTableau(tryingString, USED_ARRAY, USED_CHAR,SYMBOLE_LIST, MAX_USED_ARRAY);
	
	//Sinon si je suis au dernier caractère
	else { 
		if (tryingString[*USED_ARRAY] == SYMBOLE_LIST[strlen(SYMBOLE_LIST) - 2]) {
			while (tryingString[*USED_ARRAY] == SYMBOLE_LIST[strlen(SYMBOLE_LIST) - 2]) {
				if (toutcomplet == 0) {
					toutcomplet = 1;
					finPremierTableau(tryingString, USED_ARRAY, USED_CHAR, SYMBOLE_LIST, MAX_USED_ARRAY);
					actionAFaire(tryingString, USED_ARRAY, USED_CHAR, SYMBOLE_LIST, MAX_USED_ARRAY);
				}
				USED_CHAR[*USED_ARRAY] = 0;
				ajouterUn(tryingString, USED_ARRAY, USED_CHAR, SYMBOLE_LIST);
				*USED_ARRAY = *USED_ARRAY - 1;
			}
			ajouterUn(tryingString, USED_ARRAY, USED_CHAR, SYMBOLE_LIST);
			*USED_ARRAY = *MAX_USED_ARRAY-1;
		}
		else { // Sinon j'ajoute 1 au tableau actuel 
			ajouterUn(tryingString, USED_ARRAY, USED_CHAR, SYMBOLE_LIST);
		}
	}
}

void main()
{
	const int MAX_ALL_SIZE = 50;
	char* ARRAY_LIST[MAX_ALL_SIZE];
	char message[MAX_ALL_SIZE];
	char tryingString[MAX_ALL_SIZE];
	char SYMBOLE_LIST[] = { 'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z','1','2','3','4','5','6','7','8','9' };
	int USED_ARRAY = 0;
	int MAX_USED_ARRAY = 1;
	int ITERATION_COUNTER = 0;
	int USED_CHAR[MAX_ALL_SIZE];
	USED_CHAR[0] = 0;

	printf("\nEntrez le mot de passe a trouver : ");
	scanf("%s", message);
	printf("Recherche de %s...\n",message);

	while (strcmp(tryingString, message) != 0) 
	{
		actionAFaire(tryingString, &USED_ARRAY, USED_CHAR, SYMBOLE_LIST, &MAX_USED_ARRAY);
		ITERATION_COUNTER++;
		if (ITERATION_COUNTER % 1000000000 == 0)  printf("...\n%s\n", tryingString); //1 millard
	}
	printf("\nLe mot a été trouvé en %d tentatives !\n\n", ITERATION_COUNTER);
}
